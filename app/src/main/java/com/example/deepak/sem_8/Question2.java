package com.example.deepak.sem_8;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by deepak on 24/04/17.
 */

public class Question2 extends AppCompatActivity {

    String log = "Phase: ";
    private TextView activityLog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question2);

        activityLog = (TextView) findViewById(R.id.activityLog);

        activityLog.append("onCreate() \n");
        Log.d(log,"onCreate()");
    }

    @Override
    protected void onStart() {
        super.onStart();

        activityLog.append("onStart() \n");
        Log.d(log,"onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();

        activityLog.append("onResume() \n");
        Log.d(log,"onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();

        activityLog.append("onPause() \n");
        Log.d(log,"onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();

        activityLog.append("onStop() \n");
        Log.d(log,"onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        activityLog.append("onDestroy() \n");
        Log.d(log,"onDestroy()");
    }
}
