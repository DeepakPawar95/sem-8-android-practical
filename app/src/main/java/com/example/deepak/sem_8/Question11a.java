package com.example.deepak.sem_8;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by deepak on 24/04/17.
 */

public class Question11a extends AppCompatActivity implements View.OnClickListener {

    private EditText username;
    private EditText password;
    private Button loginBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question11a);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        loginBtn = (Button) findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String uname = username.getText().toString();
        String pwd = password.getText().toString();

        if(uname.equals("")){
            Toast.makeText(getApplicationContext(), "Please enter your username", Toast.LENGTH_SHORT).show();
            return;
        }
        if(pwd.equals("")){
            Toast.makeText(getApplicationContext(), "Please enter a password", Toast.LENGTH_SHORT).show();
            return;
        }

        if(uname.equals("sscbs") && pwd.equals("sscbs@1234")){
            Intent intent = new Intent(this, Question11b.class);
            intent.putExtra("name", uname);
            startActivity(intent);
        }
        else{
            Toast.makeText(getApplicationContext(), "Email or Password invalid", Toast.LENGTH_SHORT).show();
        }
    }
}
