package com.example.deepak.sem_8;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button ques1;
    private Button ques2;
    private Button ques3;
    private Button ques4;
    private Button ques5;
    private Button ques6;
    private Button ques7;
    private Button ques8;
    private Button ques9;
    private Button ques10;
    private Button ques11;
    private Button ques12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ques1 = (Button) findViewById(R.id.ques1);
        ques2 = (Button) findViewById(R.id.ques2);
        ques3 = (Button) findViewById(R.id.ques3);
        ques4 = (Button) findViewById(R.id.ques4);
        ques5 = (Button) findViewById(R.id.ques5);
        ques6 = (Button) findViewById(R.id.ques6);
        ques7 = (Button) findViewById(R.id.ques7);
        ques8 = (Button) findViewById(R.id.ques8);
        ques9 = (Button) findViewById(R.id.ques9);
        ques10 = (Button) findViewById(R.id.ques10);
        ques11 = (Button) findViewById(R.id.ques11);
        ques12 = (Button) findViewById(R.id.ques12);
        ques1.setOnClickListener(this);
        ques2.setOnClickListener(this);
        ques3.setOnClickListener(this);
        ques4.setOnClickListener(this);
        ques5.setOnClickListener(this);
        ques6.setOnClickListener(this);
        ques7.setOnClickListener(this);
        ques8.setOnClickListener(this);
        ques9.setOnClickListener(this);
        ques10.setOnClickListener(this);
        ques11.setOnClickListener(this);
        ques12.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent intent;

        switch (v.getId()){
            case R.id.ques1: {
                intent = new Intent(this, Question1.class);
                startActivity(intent);
                break;
            }
            case R.id.ques2: {
                intent = new Intent(this, Question2.class);
                startActivity(intent);
                break;
            }
            case R.id.ques3: {
                intent = new Intent(this, Question3a.class);
                startActivity(intent);
                break;
            }
            case R.id.ques4: {
                intent = new Intent(this, Question4a.class);
                startActivity(intent);
                break;
            }
            case R.id.ques5: {
                intent = new Intent(this, Question5.class);
                startActivity(intent);
                break;
            }
            case R.id.ques6: {
                intent = new Intent(this, Question6.class);
                startActivity(intent);
                break;
            }
            case R.id.ques7: {
                intent = new Intent(this, Question7.class);
                startActivity(intent);
                break;
            }
            case R.id.ques8: {
                intent = new Intent(this, Question8.class);
                startActivity(intent);
                break;
            }
            case R.id.ques9: {
                intent = new Intent(this, Question9.class);
                startActivity(intent);
                break;
            }
            case R.id.ques10: {
                intent = new Intent(this, Question10.class);
                startActivity(intent);
                break;
            }
            case R.id.ques11: {
                intent = new Intent(this, Question11a.class);
                startActivity(intent);
                break;
            }
            case R.id.ques12: {
                intent = new Intent(this, Question12.class);
                startActivity(intent);
                break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
