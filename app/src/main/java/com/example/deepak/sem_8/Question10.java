package com.example.deepak.sem_8;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by deepak on 24/04/17.
 */

public class Question10 extends AppCompatActivity implements View.OnClickListener {

    private EditText username;
    private EditText password;
    private Button loginBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question10);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        loginBtn = (Button) findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String uname = username.getText().toString();
        String pwd = password.getText().toString();

        if(uname.equals("")){
            Toast.makeText(getApplicationContext(), "Please enter your username", Toast.LENGTH_SHORT).show();
            return;
        }
        if(pwd.equals("")){
            Toast.makeText(getApplicationContext(), "Please enter a password", Toast.LENGTH_SHORT).show();
            return;
        }

        if(uname.equals("sscbs") && pwd.equals("sscbs@1234")){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Welcome " + uname);
            builder.create();
            builder.show();

            //Toast.makeText(getApplicationContext(), "Welcome " + uname, Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getApplicationContext(), "Email or Password invalid", Toast.LENGTH_SHORT).show();
        }
    }
}
