package com.example.deepak.sem_8;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

/**
 * Created by deepak on 24/04/17.
 */

public class Question7 extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    private RadioGroup courseChoice;
    private RadioButton btech;
    private RadioButton bms;
    private RadioButton bsc;
    private RadioButton bbs;
    private TextView teacher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question7);

        btech = (RadioButton) findViewById(R.id.btech);
        bms = (RadioButton) findViewById(R.id.bms);
        bsc = (RadioButton) findViewById(R.id.bsc);
        bbs = (RadioButton) findViewById(R.id.bbs);

        courseChoice = (RadioGroup) findViewById(R.id.courseChoice);
        courseChoice.setOnCheckedChangeListener(this);

        teacher = (TextView) findViewById(R.id.teacher);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        if (checkedId == btech.getId()){
            teacher.setText(R.string.btech_teacher);
        }
        if (checkedId == bms.getId()){
            teacher.setText(R.string.bms_teacher);
        }
        if (checkedId == bsc.getId()){
            teacher.setText(R.string.bsc_teacher);
        }
        if (checkedId == bbs.getId()){
            teacher.setText(R.string.bbs_teacher);
        }
    }
}
