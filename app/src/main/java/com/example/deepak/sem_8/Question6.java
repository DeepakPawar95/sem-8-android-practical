package com.example.deepak.sem_8;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by deepak on 24/04/17.
 */

public class Question6 extends AppCompatActivity {

    private TextView selectedItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question6);

        selectedItem = (TextView) findViewById(R.id.selectedItem);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_q6, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.facebook) {
            selectedItem.setText(R.string.facebook);
        }
        if (id == R.id.twitter) {
            selectedItem.setText(R.string.twitter);
        }
        if (id == R.id.instagram) {
            selectedItem.setText(R.string.instagram);
        }
        if (id == R.id.gmail) {
            selectedItem.setText(R.string.gmail);
        }
        if (id == R.id.whatsapp) {
            selectedItem.setText(R.string.whatsapp);
        }

        return super.onOptionsItemSelected(item);
    }
}
