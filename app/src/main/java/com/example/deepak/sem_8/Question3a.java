package com.example.deepak.sem_8;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by deepak on 24/04/17.
 */

public class Question3a extends AppCompatActivity {

    private EditText userText;
    private Button sendBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question3a);

        userText = (EditText) findViewById(R.id.userText);
        sendBtn = (Button) findViewById(R.id.sendBtn);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = userText.getText().toString();

                Intent intent = new Intent(getApplicationContext(), Question3b.class);
                intent.putExtra("data", data);
                startActivity(intent);
            }
        });
    }
}
