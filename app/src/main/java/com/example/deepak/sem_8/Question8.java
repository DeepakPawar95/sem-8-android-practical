package com.example.deepak.sem_8;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * Created by deepak on 24/04/17.
 */

public class Question8 extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout bgView;
    private Button redBtn;
    private Button blueBtn;
    private Button tealBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question8);

        bgView = (LinearLayout) findViewById(R.id.bgView);

        redBtn = (Button) findViewById(R.id.redBtn);
        blueBtn = (Button) findViewById(R.id.blueBtn);
        tealBtn = (Button) findViewById(R.id.tealBtn);
        redBtn.setOnClickListener(this);
        blueBtn.setOnClickListener(this);
        tealBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.redBtn){
            bgView.setBackgroundColor(getResources().getColor(R.color.redColor));
        }
        if (v.getId() == R.id.blueBtn){
            bgView.setBackgroundColor(getResources().getColor(R.color.blueColor));
        }
        if (v.getId() == R.id.tealBtn){
            bgView.setBackgroundColor(getResources().getColor(R.color.tealColor));
        }
    }
}
