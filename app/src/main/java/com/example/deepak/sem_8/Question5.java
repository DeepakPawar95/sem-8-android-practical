package com.example.deepak.sem_8;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

/**
 * Created by deepak on 24/04/17.
 */

public class Question5 extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Spinner countriesSpinner;
    private ImageView countryImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question5);

        countryImage = (ImageView) findViewById(R.id.countryImage);

        countriesSpinner = (Spinner) findViewById(R.id.countries);
        countriesSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (position){
            case 0:{
                countryImage.setImageResource(R.drawable.india_flag);
                break;
            }
            case 1:{
                countryImage.setImageResource(R.drawable.australia_flag);
                break;
            }
            case 2:{
                countryImage.setImageResource(R.drawable.england_flag);
                break;
            }
            case 3:{
                countryImage.setImageResource(R.drawable.usa_flag);
                break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
