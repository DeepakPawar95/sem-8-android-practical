package com.example.deepak.sem_8;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by deepak on 24/04/17.
 */

public class Question4b extends AppCompatActivity {

    private TextView getText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question4b);

        Bundle extras = getIntent().getExtras();

        String data = extras.getString(Intent.EXTRA_TEXT);

        getText = (TextView) findViewById(R.id.getText);
        getText.setText(data);
    }
}
