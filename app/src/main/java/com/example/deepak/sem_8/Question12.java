package com.example.deepak.sem_8;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepak on 24/04/17.
 */

public class Question12 extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private DbHelper dbHelper;
    private SQLiteDatabase db_read, db_write;

    private Button insertBtn;
    private EditText listInput;
    private ListView listView;
    Context context;

    ArrayAdapter<String> adapter;

    List noteIdsList = new ArrayList<>();
    List notesList = new ArrayList<String>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question12);

        context = this;

        dbHelper = new DbHelper(context);
        db_read = dbHelper.getReadableDatabase();
        db_write = dbHelper.getWritableDatabase();

        listInput = (EditText) findViewById(R.id.listInput);

        insertBtn = (Button) findViewById(R.id.insertBtn);
        insertBtn.setOnClickListener(this);

        listView = (ListView) findViewById(R.id.listView);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, notesList);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        readData();
    }

    private void readData() {

        String[] projection = {
                DbHelper.COLUMN_ID,
                DbHelper.COLUMN_NOTE
        };

        String sortOrder = DbHelper.COLUMN_ID + " DESC";

        Cursor cursor = db_read.query(
                DbHelper.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                sortOrder
        );

        noteIdsList.clear();
        notesList.clear();

        while (cursor.moveToNext()) {
            long itemId = cursor.getLong(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_ID));
            String note = cursor.getString(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_NOTE));

            noteIdsList.add(itemId);
            notesList.add(note);
        }

        adapter.notifyDataSetChanged();

        cursor.close();

    }

    private void editNote(long id, String note) {

        ContentValues values = new ContentValues();
        values.put(DbHelper.COLUMN_NOTE, note);

        String selection = DbHelper.COLUMN_ID + " = ?";
        String[] selectionArgs = {String.valueOf(id)};

        int count = db_read.update(
                DbHelper.TABLE_NAME,
                values,
                selection,
                selectionArgs
        );
        readData();
    }

    private void deleteNote(long id) {
        String selection = DbHelper.COLUMN_ID + " = ?";
        String[] selectionArgs = {String.valueOf(id)};
        db_write.delete(DbHelper.TABLE_NAME, selection, selectionArgs);
        readData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db_read.close();
        db_write.close();
        dbHelper.close();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.insertBtn){
            String data = listInput.getText().toString();

            if (data.isEmpty()) {
                Toast.makeText(context, "Enter a Note Text", Toast.LENGTH_SHORT).show();
                return;
            }

            ContentValues values = new ContentValues();
            values.put(DbHelper.COLUMN_NOTE, data);
            db_write.insert(DbHelper.TABLE_NAME, null, values);

            listInput.setText("");
            readData();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        final long dbPos = (long) noteIdsList.get(position);
        String itemValue = (String) notesList.get(position);


        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.dialog_edit_note, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.dialog_notes_edit_text);
        Button deleteButton = (Button) promptsView.findViewById(R.id.dialog_delete_button);
        Button updateButton = (Button) promptsView.findViewById(R.id.dialog_update_button);
        Button cancelButton = (Button) promptsView.findViewById(R.id.dialog_cancel_button);


        // set dialog message
        alertDialogBuilder
                .setCancelable(true);

        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        userInput.setText(itemValue);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.dialog_delete_button:
                        deleteNote(dbPos);
                        alertDialog.dismiss();
                        break;
                    case R.id.dialog_update_button:
                        String note = userInput.getText().toString();
                        if (note.isEmpty()) {
                            Toast.makeText(v.getContext(), "Enter a Note Text", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        editNote(dbPos, note);
                        alertDialog.dismiss();

                        break;
                    case R.id.dialog_cancel_button:
                        alertDialog.dismiss();
                        break;
                }
            }
        };

        deleteButton.setOnClickListener(listener);
        updateButton.setOnClickListener(listener);
        cancelButton.setOnClickListener(listener);

    }
}
